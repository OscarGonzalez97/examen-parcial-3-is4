<?php

namespace App;

use DateTime;

class Autor
{
    public string $nombre;
    public string $nacionalidad;
    public DateTime $f_nacimiento;

    function __construct($nombre, $nacionalidad, $f_nacimiento)
    {
        $this->nombre = $nombre;
        $this->nacionalidad = $nacionalidad;
        $this->f_nacimiento = $f_nacimiento;
    }

}