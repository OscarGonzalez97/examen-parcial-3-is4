<?php

namespace App;

class Libro
{

    public string $titulo;
    public TipoLibro $tipo_libro;
    public string $editorial;
    public int $anho;
    public int $ISBN;
    public array|Autor $autores = [];

    function __construct($titulo, $tipo_libro, $editorial, $anho, $ISBN, $autores)
    {
        $this->titulo = $titulo;
        $this->tipo_libro = $tipo_libro;
        $this->editorial = $editorial;
        $this->anho = $anho;
        $this->ISBN = $ISBN;
        $this->autores = $autores;
    }
    /**
     * @return Autor
     */
    public function getNombreAutor()
    {
        return $this->autores;
    }

}