<?php

namespace App;

enum TipoLibro
{
    case Terror;
    case Drama;
    case Comedia;
    case Scifi;
}