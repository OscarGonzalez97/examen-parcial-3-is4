<?php

use App\Autor;
use App\Libro;
use App\TipoLibro;

require 'app/bootstrap.php';
$autor1 = new Autor('Oscar G.', 'Paraguaya', new DateTime('2000-01-01'));
$autor2 = new Autor('Tomas A.', 'Paraguaya', new DateTime('2000-01-01'));
$autor3 = new Autor('Renato F.', 'Paraguaya', new DateTime('2000-01-01'));

$autores = array($autor1, $autor2, $autor3);

$libro = new Libro(
    'Las aventuras de IS4',
    TipoLibro::Terror,
    'La UCA',
    2022,
    1239481203847,
    $autores
);


print_r($libro->getNombreAutor());